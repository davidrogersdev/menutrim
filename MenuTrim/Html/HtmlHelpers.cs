﻿using System.Collections.Generic;
using System;
using System.Web;
using System.Web.Mvc;
using MenuTrim.Models;
using System.Text;
using System.Security.Claims;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;

namespace MenuTrim.Html
{
    public static class HtmlHelpers
    {
        private const string TrueDat = "true";

        public static IHtmlString BootstrapMenu(this HtmlHelper helper, MenuAuthModel model)
        {
            System.Diagnostics.Stopwatch watch = new Stopwatch();
            watch.Start();

            var httpContext = helper.ViewContext.HttpContext;
            var user = httpContext.User;

            var menubuilder = new TagBuilder("div");
            menubuilder.AddCssClass("navbar-fixed-top");
            menubuilder.AddCssClass("navbar-inverse");
            menubuilder.AddCssClass("navbar");

            var navHeaderDiv = new TagBuilder("div");
            navHeaderDiv.AddCssClass("navbar-header");

            var containerDiv = new TagBuilder("div");
            containerDiv.AddCssClass("container");

            var navCollapseDiv = new TagBuilder("div");
            navCollapseDiv.AddCssClass("navbar-collapse collapse ");

            var listBuilderDiv = new TagBuilder("ul");
            listBuilderDiv.AddCssClass("nav navbar-nav");
            
            int menuItemmCount;
            listBuilderDiv.InnerHtml += BuildListItems(model, user, out menuItemmCount);
            navCollapseDiv.InnerHtml += listBuilderDiv.ToString(TagRenderMode.Normal);

            var buttonBuilder = new TagBuilder("button");
            buttonBuilder.AddCssClass("navbar-toggle");
            
            buttonBuilder.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(new
            {
                data_toggle = "collapse", data_target = ".navbar-collapse", type = "button"
            }));

            // now, construct the menu
            buttonBuilder.InnerHtml += BuildHamburgerSlices(menuItemmCount);
            navHeaderDiv.InnerHtml += buttonBuilder.ToString(TagRenderMode.Normal);
            containerDiv.InnerHtml += navHeaderDiv.ToString(TagRenderMode.Normal);
            containerDiv.InnerHtml += navCollapseDiv.ToString(TagRenderMode.Normal);
            menubuilder.InnerHtml += containerDiv.ToString(TagRenderMode.Normal);

            watch.Stop();
            Debug.WriteLine("Helper : {0}s", watch.ElapsedTicks);
            Debug.WriteLine("Helper : {0}s", (decimal)watch.ElapsedTicks / (decimal)TimeSpan.TicksPerMillisecond);
            return new MvcHtmlString(menubuilder.ToString(TagRenderMode.Normal));
        }

        private static string BuildHamburgerSlices(int menuItemmCount)
        {
            var sb = new StringBuilder();

            for (int i = 0; i < menuItemmCount; i++)
            {
                var spanBuilder = new TagBuilder("span");
                spanBuilder.AddCssClass("icon-bar");
                sb.Append(spanBuilder.ToString(TagRenderMode.Normal));
            }

            return sb.ToString();
        }

        private static IHtmlString BuildListItems(MenuAuthModel model, IPrincipal userPrincipal, out int menuItemCount)
        {            
            var stringBuilder = new StringBuilder();
            menuItemCount = 0;

            /* Links Not Requiring that the User even be Logged In */
            foreach (var linkClaim in model.SiteAccessModel.LinksNotRequiringUserLoggedIn)
            {
                RenderLinkFromKeyValuePair(ref menuItemCount, linkClaim, stringBuilder);
            }

            if (userPrincipal.Identity.IsAuthenticated)
            {
                /* Links for which the user only requires one of the claims */
                foreach (var linkClaim in model.SiteAccessModel.LinksWithAnyRequiredClaims)
                {
                    // get all claims from the list which the user does have (if any)
                    var claimsInListWhichUserHas =
                        linkClaim.Value.Intersect(model.UserClaims, new LinkClaimComparer()).ToList();

                    // make sure at least 1 claim has a value of true
                    if (claimsInListWhichUserHas.Any() &&
                        claimsInListWhichUserHas.Any(c => c.Value.Equals(TrueDat, StringComparison.OrdinalIgnoreCase)))
                    {
                        RenderLinkFromClaim(ref menuItemCount, linkClaim, stringBuilder);
                    }
                }

                /* Links for which the user requires all of the claims in the collection */
                foreach (var linkClaim in model.SiteAccessModel.LinksWithAllRequiredClaims)
                {
                    // make sure user has ALL claims
                    if (!linkClaim.Value.Except(model.UserClaims, new LinkClaimComparer()).Any())
                    {
                        // make sure all claims have a value of true
                        if (linkClaim.Value.All(c => c.Value.Equals(TrueDat, StringComparison.OrdinalIgnoreCase)))
                        {
                            RenderLinkFromClaim(ref menuItemCount, linkClaim, stringBuilder);
                        }
                    }
                }

                /* Links for which the user does not require any claims (although they still need to be logged in) */
                foreach (var linkClaim in model.SiteAccessModel.LinksNotRequiringAnyClaims)
                {
                    RenderLinkFromKeyValuePair(ref menuItemCount, linkClaim, stringBuilder);
                }
            }

            return new MvcHtmlString(stringBuilder.ToString());
        }

        private static void RenderLinkFromClaim(ref int menuItemCount, KeyValuePair<KeyValuePair<string, string>, IList<Claim>> linkClaim, StringBuilder stringBuilder)
        {
            TagBuilder helper;
            helper = new TagBuilder("li");
            helper.InnerHtml += "<a href='" + linkClaim.Key.Value + "'>" + linkClaim.Key.Key + "</a>";
            stringBuilder.Append(helper.ToString(TagRenderMode.Normal));
            menuItemCount++;
        }

        private static void RenderLinkFromKeyValuePair(ref int menuItemCount, KeyValuePair<string, string> linkClaim, StringBuilder stringBuilder)
        {
            TagBuilder helper;
            helper = new TagBuilder("li");
            helper.InnerHtml += "<a href='" + linkClaim.Value + "'>" + linkClaim.Key + "</a>";
            stringBuilder.Append(helper.ToString(TagRenderMode.Normal));
            menuItemCount++;
        }

        /// <summary>
        /// Helper class to help with set operations. Need a reliable and fast comparer.
        /// </summary>
        class LinkClaimComparer : IEqualityComparer<Claim>
        {
            public bool Equals(Claim x, Claim y)
            {
                return x.Type.Equals(y.Type, StringComparison.OrdinalIgnoreCase);
            }

            public int GetHashCode(Claim obj)
            {
                return obj.Type.GetHashCode();
            }
        }
    }
}