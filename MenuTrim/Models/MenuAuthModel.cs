﻿using System.Collections.Generic;
using System.Security.Claims;

namespace MenuTrim.Models
{
    public class MenuAuthModel
    {
        public SiteAccessModel SiteAccessModel { get; set; }

        public IEnumerable<Claim> UserClaims { get; set; }
    }

    public class SiteAccessModel
    {
        // Links which require ALL of the accompanying permissions
        public IDictionary<KeyValuePair<string, string>, IList<Claim>> LinksWithAllRequiredClaims { get; set; }

        // Links which require at least 1 of the accompanying permissions
        public IDictionary<KeyValuePair<string, string>, IList<Claim>> LinksWithAnyRequiredClaims { get; set; }

        // Links which don't require any permissions
        public IDictionary<string, string> LinksNotRequiringAnyClaims { get; set; }

        public IDictionary<string, string> LinksNotRequiringUserLoggedIn { get; set; }

    }

}