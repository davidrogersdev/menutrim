﻿using MenuTrim.Models;
using System.Collections.Generic;
using System.Security.Claims;
using System.Web.Mvc;

namespace MenuTrim.Controllers
{
    public class HomeController : Controller
    {
        private const string SMHClaim = "http://sydneyMorningHeraldClaim";
        private const string GuardianClaim = "http://guardianClaim";
        private const string GoogleClaim = "http://googleClaim";
        private const string YahooClaim = "http://yahooClaim";
        private const string TwitterClaim = "http://twitterClaim";
        private const string FacebookClaim = "http://facebookClaim";
        private const string RandomClaim = "http://randomClaim";

        public ActionResult Index()
        {
            var menuModel = new MenuAuthModel
            {
                SiteAccessModel = new SiteAccessModel
                {
                    LinksWithAllRequiredClaims = new Dictionary<KeyValuePair<string, string>, IList<Claim>>(),

                    LinksWithAnyRequiredClaims = new Dictionary<KeyValuePair<string, string>, IList<Claim>>
                    {
                        {new KeyValuePair<string, string>("Google","http://www.google.com"),
                            new List<Claim>
                            {
                                new Claim(GoogleClaim, "true"),
                                new Claim(YahooClaim, "true"),
                                new Claim(RandomClaim, "true")
                            }
                        },
                        {new KeyValuePair<string, string>("Yahoo","http://www.yahoo.com"), new List<Claim> { new Claim(YahooClaim, "true") } },
                        {new KeyValuePair<string, string>("Twitter","http://www.twitter.com"), new List<Claim> { new Claim(TwitterClaim, "true") } },
                        {new KeyValuePair<string, string>("Facebook","http://www.facebook.com"), new List<Claim> { new Claim(FacebookClaim, "true") } }
                    },
                    LinksNotRequiringAnyClaims = new Dictionary<string, string>
                    {
                        {"Dave", "http://davidrogers.id.au" }
                    },
                    LinksNotRequiringUserLoggedIn = new Dictionary<string, string>
                    {
                        {"Anonymous", "http://www.asp.net" }
                    }
                }
                ,
                UserClaims = new List<Claim>
                {
                    new Claim(GoogleClaim, "true"),
                    new Claim(YahooClaim, "true"),
                    new Claim(TwitterClaim, "true"),
                    new Claim(GuardianClaim, "true"),
                    new Claim(SMHClaim, "true")
                }
            };


            menuModel.SiteAccessModel.LinksWithAllRequiredClaims.Add(
                new KeyValuePair<string, string>("Sydney Morning Herald", "http://www.smh.com.au"),
                new List<Claim>
                {
                    new Claim(GoogleClaim, "true"),
                    new Claim(SMHClaim, "true"),
                    new Claim(TwitterClaim, "true"),
                    new Claim(YahooClaim, "true")
                    //new Claim(FacebookClaim, "true")
                }
            );

            menuModel.SiteAccessModel.LinksWithAllRequiredClaims.Add(
                new KeyValuePair<string, string>("Gurdian", "http://www.theguardian.com/au"),
                new List<Claim>
                {
                    new Claim(GoogleClaim, "true"),
                    new Claim(SMHClaim, "true"),
                    new Claim(TwitterClaim, "true"),
                    new Claim(GuardianClaim, "false"),
                    new Claim(YahooClaim, "true")
                }
            );

            return View(menuModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}